import { fromJS } from 'immutable';
import CLOCK_TYPES from './ClockTypes';

const initialState = fromJS({
	clockedIn: false,
	clockedInTime: null,
	currentTime: null,
	clockInVerified: null,
	clockOutVerified: null,
});

export default function clockReducer(state = initialState, action) {
	switch (action.type) {
	case CLOCK_TYPES.CLOCK_IN: {
		console.log('REDUX CLOCK_IN');
		const clockedInTime = action.payload;
		return state
			.set('clockedIn', true)
			.set('clockedInTime', clockedInTime)
			.set('currentTime', clockedInTime);
	}
	case CLOCK_TYPES.CLOCK_OUT: {
		console.log('REDUX CLOCK_OUT');
		const clockedOutTime = action.payload;
		return state
			.set('clockedIn', false)
			.set('currentTime', clockedOutTime);
	}
	case CLOCK_TYPES.UPDATE_TIME: {
		console.log('REDUX UPDATE_TIME');
		const currentTime = action.payload;
		return state.set('currentTime', currentTime);
	}
	case CLOCK_TYPES.VERIFY_CLOCK_IN_FAIL: {
		return state.set('clockInVerified', false)
	}
	case CLOCK_TYPES.VERIFY_CLOCK_IN_SUCCESS: {
		return state.set('clockInVerified', true);
	}
	case CLOCK_TYPES.VERIFY_CLOCK_OUT_FAIL: {
		return state.set('clockOutVerified', false)
	}
	case CLOCK_TYPES.VERIFY_CLOCK_OUT_SUCCESS: {
		return state.set('clockOutVerified', true);
	}
	case CLOCK_TYPES.RESET_CLOCK: {
		return initialState;
	}
	default: {
		return state;
	}
	}
}
