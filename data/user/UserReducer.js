import { fromJS } from 'immutable';
import USER_TYPES from './UserTypes';

const initialState = fromJS({
	id: null,
	fullName: null,
	email: null,
	isAuthenticated: false,
	error: null,
	loadingLogin: false,
	loadingLogout: false,
	loadingSignUp: false,
});

export default function userReducer(state = initialState, action) {
	switch (action.type) {
	case USER_TYPES.LOGIN: {
		return state.set('loadingLogin', true);
	}
	case USER_TYPES.LOGIN_FAIL: {
		return state
			.set('loadingLogin', false)
			.set('error', action.payload);
	}
	case USER_TYPES.LOGIN_SUCCESS: {
		const {
			email,
			id,
		} = action.payload;
		return state
			.set('loadingLogin', false)
			.set('id', id)
			.set('email', email);
	}
	case USER_TYPES.LOGOUT: {
		return state.set('loadingLogout', true);
	}
	case USER_TYPES.LOGOUT_FAIL: {
		return state
			.set('loadingLogout', false)
			.set('error', action.payload);
	}
	case USER_TYPES.LOGOUT_SUCCESS: {
		return state
			.set('loadingLogout', false)
			.set('isAuthenticated', false);
	}
	case USER_TYPES.SIGN_UP: {
		return state.set('loadingSignUp', true);
	}
	case USER_TYPES.SIGN_UP_FAIL: {
		return state
			.set('loadingSignUp', false)
			.set('error', action.payload);
	}
	case USER_TYPES.SIGN_UP_SUCCESS: {
		return state.set('loadingSignUp', false);
	}
	default: {
		return state;
	}
	}
}
