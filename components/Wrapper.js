import React from 'react';
import styled from 'styled-components';

export const PaddingWrapper = styled.View`
	padding-top: ${props => props.top || 0};
	padding-left: ${props => props.left || 0};
	padding-right: ${props => props.right || 0};
	padding-bottom: ${props => props.bottom || 0};
`;
