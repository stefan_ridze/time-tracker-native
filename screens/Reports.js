import React, { Fragment, useEffect } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';

// Actions
import { logout } from '../data/user/UserActions';
import { fetchReports } from '../data/reports/ReportsActions';

// pictures
import circleSmall from '../public/pictures/circle-small.png';

// Style
import Colors from '../style/Colors';
import FontSizes from '../style/FontSizes';

// Components
import Header from '../components/Header';
import ReportRow from '../components/ReportRow';
import CurrentDate from '../components/CurrentDate';
import { BaseDivider } from '../components/Divider';
import { Heading } from '../components/Text';
import { FlexRowSpaceAround, FlexRowSpaceBetween } from '../components/Flex';

const ReportsWrapper = styled.ScrollView`
	width: 100%;
	background: ${Colors.white};
	padding: 0 20px 15px 20px;
`;

const ReportPanelHeading = styled.Text`
	font-size: ${FontSizes.base};
	color: ${Colors.darkGrey};
	font-weight: bold;
	padding: 10px 0;
`;

const ReportPanelWrapper = styled.View`
	margin-bottom: 30px;
`;

const Reports = (props) => {
	const {
		reports, actions, navigation,
	} = props;

	const { navigate } = navigation;

	useEffect(() => {
		actions.fetchReports();
	}, []);

	const renderReports = (dailyReports) => {
		return dailyReports.map((report, index) => {
			return (
				<ReportRow
					startTime={report.clockedInTime}
					endTime={report.clockedOutTime}
					total={report.sum}
					key={report.id}
					greenBorder={index + 1 === dailyReports.length}
				/>
			);
		});
	};

	const renderReportPanel = (date, dailyReports, total) => {
		return (
			<ReportPanelWrapper key={date}>
				<ReportPanelHeading>
					{`Time report for ${date}:`}
				</ReportPanelHeading>
				{renderReports(dailyReports)}
				<FlexRowSpaceBetween>
					<ReportPanelHeading>Total:</ReportPanelHeading>
					<ReportPanelHeading>{total}</ReportPanelHeading>
				</FlexRowSpaceBetween>
			</ReportPanelWrapper>
		);
	};

	const renderAllReports = () => {
		return reports.toJS().map((dailyReportsData) => {
			const {
				date,
				dailyReports,
				total,
			} = dailyReportsData;
			return renderReportPanel(date, dailyReports, total);
		});
	};

	const signOut = async () => {
		await actions.logout();
		navigate('SignIn');
	};

	const goBack = () => navigation.goBack();

	const navigationStack = navigation.dangerouslyGetParent();
	const canGoBack = navigationStack && navigationStack.state && navigationStack.state.index !== 1;

	return (
		<Fragment>
			<Header
				hasBackButton={canGoBack}
				onBackButtonClick={goBack}
				onSignOut={signOut}
				picture={circleSmall}
				width="236px"
				height="61px"
			/>
			<BaseDivider height="20px" />
			<ReportsWrapper>
				<CurrentDate />
				<BaseDivider height="30px" />
				<FlexRowSpaceAround>
					<Heading>REPORTS</Heading>
				</FlexRowSpaceAround>
				<BaseDivider height="40px" />
				{renderAllReports()}
			</ReportsWrapper>
		</Fragment>
	);
};

function mapStateToProps(state) {
	return {
		reports: state.getIn(['reports', 'reports']),
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators({
			fetchReports,
			logout,
		}, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Reports);
