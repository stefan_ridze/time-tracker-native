import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';

// Actions
import { clockIn, clockOut, updateTime } from '../data/clock/ClockActions';

// Style
import Colors from '../style/Colors';
import FontSizes from '../style/FontSizes';

// Components
import Clock from '../components/Clock';
import BaseButton from '../components/BaseButton';
import { BaseDivider } from '../components/Divider';
import { BaseText, Heading } from '../components/Text';
import { FlexRowSpaceAround } from '../components/Flex';

let clockInterval = null;

const Dashboard = (props) => {
	const { actions, navigation } = props;
	const { navigate } = navigation;

	const getCurrentTime = () => moment().format();

	const incrementTime = () => actions.updateTime(getCurrentTime());

	const clockIn = () => {
		actions.clockIn(getCurrentTime());
		clockInterval = setInterval(incrementTime, 1000);
	};

	const clockOut = () => {
		clearInterval(clockInterval);
		clockInterval = null;
		actions.clockOut(getCurrentTime());
	};

	const {
		clockedIn,
	} = props;

	return (
		<Fragment>
			<Heading>TOTAL TIME SPENT TODAY:</Heading>
			<BaseDivider height="40px" />
			<FlexRowSpaceAround>
				<Clock />
			</FlexRowSpaceAround>
			<BaseDivider height="40px" />
			<BaseButton
				bigger
				backgroundColor={Colors.lighterGrey}
				title="REPORTS"
				onPress={() => navigate('Reports')}
			/>
			<BaseDivider height="10px" />
			<BaseButton
				bigger
				title={clockedIn ? 'CLOCK_OUT' : 'CLOCK_IN'}
				onPress={clockedIn ? clockOut : clockIn}
				backgroundColor={clockedIn && Colors.baseRed}
			/>
			<BaseDivider height="15px" />
			<FlexRowSpaceAround>
				<BaseText
					color={Colors.lightGrey}
					fontSize={FontSizes.small}
				>
					Clicking
					<BaseText
						color={Colors.mediumGrey}
						fontSize={FontSizes.small}
						fontWeight="bold"
					> {clockedIn ? 'CLOCK OUT' : 'CLOCK IN'}
					</BaseText> button will {clockedIn ? 'stop' : 'start'} the time counter.
				</BaseText>
			</FlexRowSpaceAround>
		</Fragment>
	);
};

function mapStateToProps(state) {
	return {
		clockedIn: state.getIn(['clock', 'clockedIn']),
		clockInVerified: state.getIn(['clock', 'clockInVerified']),
	}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators({
			clockIn,
			clockOut,
			updateTime,
		}, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
