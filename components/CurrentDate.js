import React from 'react';
import moment from 'moment';

// Styles
import FontSizes from '../style/FontSizes';
import Colors from '../style/Colors';

// Components
import { FlexRow } from './Flex';
import { BaseText } from './Text';

const CurrentDate = () => {
	const formatDate = date => date.split(', ').slice(0, 2);

	const dateWords = formatDate(moment().format('LLLL'));

	return (
		<FlexRow>
			<BaseText fontSize={FontSizes.medium} color={Colors.darkGrey}>
				{dateWords[0]}
			</BaseText>
			<BaseText fontSize={FontSizes.medium} color={Colors.lightGrey}>
				{` ${dateWords[1]}`}
			</BaseText>
		</FlexRow>
	);
};

export default CurrentDate;
