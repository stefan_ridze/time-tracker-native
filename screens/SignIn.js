import React, { Fragment, useState } from 'react';
import styled from 'styled-components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Api from '../lib/api';

// Actions
import { login } from '../data/user/UserActions';

// pictures
import logoSmall from '../public/pictures/logo-small.png';

// style
import Colors from '../style/Colors';
import FontSizes from '../style/FontSizes';

// components
import TopPicture from '../components/TopPicture';
import BaseButton from '../components/BaseButton';
import BaseInput from '../components/BaseInput';

import { FlexRow } from '../components/Flex';
import { BaseDivider } from '../components/Divider';
import { PaddingWrapper } from '../components/Wrapper';
import { BaseText } from '../components/Text';

const SignUpWrapper = styled.ScrollView`
	width: 100%;
	min-height: 100%;
	background: ${Colors.baseDark};
	padding: 0 20px 15px 20px;
`;

const HeadingWrapper = styled.View`
	color: ${Colors.baseGreen};
	flex-direction: row;
	justify-content: space-around;
`;

const Heading = styled.Text`
	color: ${props => props.white ? Colors.white : Colors.baseGreen};
	margin-left: ${props => (props.white ? '4px' : '0px')};
	font-size: ${FontSizes.big};
	font-weight: bold;
`;

const BottomTextWrapper = styled(FlexRow)`
	width: 100%;
	justify-content: space-around;
	margin-top: 10px;
`;

const GreenLink = styled(BaseText)`
	color: ${Colors.baseGreen};
`;

const SignIn = (props) => {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const { navigate } = props.navigation;

	const resetInputs = () => {
		setEmail('');
		setPassword('');
	};

	const onSubmit = async () => {
		const { actions } = props;
		try {
			await actions.login(email, password);
			resetInputs();
			navigate('Dashboard');
		} catch (error) {
			console.log('error', error);
		}
	};

	return (
		<Fragment>
			<TopPicture source={logoSmall} />
			<SignUpWrapper>
				<HeadingWrapper>
					<FlexRow>
						<Heading>TIME</Heading>
						<Heading white>TRACK.</Heading>
					</FlexRow>
				</HeadingWrapper>
				<BaseDivider height={30} />
				<PaddingWrapper bottom={5}>
					<BaseInput
						value={email}
						onChangeText={setEmail}
						label="EMAIL"
						placeholder="example@email.com"
					/>
				</PaddingWrapper>
				<PaddingWrapper>
					<BaseInput
						secureTextEntry
						value={password}
						onChangeText={setPassword}
						placeholder="5+ Characters"
						label="PASSWORD"
					/>
				</PaddingWrapper>
				<BaseDivider height="60px" />
				<BaseButton
					color={Colors.baseGreen}
					onPress={onSubmit}
					title="SIGN IN"
				/>
				<BottomTextWrapper>
					<FlexRow>
						<BaseText>Not a member?</BaseText>
						<PaddingWrapper left={3}>
							<GreenLink onPress={() => navigate('SignUp')}>SIGN UP</GreenLink>
						</PaddingWrapper>
					</FlexRow>
				</BottomTextWrapper>
			</SignUpWrapper>
		</Fragment>
	);
};

function mapStateToProps(state) {
	return {
		email: state.get('user').get('email'),
	}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators({
			login,
		}, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
