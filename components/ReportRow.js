import React from 'react';
import styled from 'styled-components';

// Styles
import FontSizes from '../style/FontSizes';
import Colors from '../style/Colors';

// Components
import { FlexRow, FlexRowSpaceBetween } from './Flex';

const Row = styled(FlexRowSpaceBetween)`
	width: 100%;
	padding: 5px 0;
	border-bottom-color: ${props => (props.greenBorder ? Colors.baseGreen : Colors.lighterGrey)};
	border-bottom-width: 1px;
`;

const Time = styled.Text`
	font-size: ${FontSizes.base};
	color: ${Colors.mediumGrey};
`;

const IntervalWrapper = styled(FlexRow)``;


const ReportRow = ({ startTime, endTime, total, greenBorder }) => {
	return (
		<Row greenBorder={greenBorder}>
			<IntervalWrapper>
				<Time>{startTime}</Time>
				<Time> - </Time>
				<Time>{endTime }</Time>
			</IntervalWrapper>
			<Time>{total}</Time>
		</Row>
	);
};

export default ReportRow;
