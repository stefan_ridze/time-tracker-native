import {
	createStore,
	applyMiddleware,
	compose,
} from 'redux';

import thunk from 'redux-thunk';
import Immutable from 'immutable';
import logger from 'redux-logger';

import reducers from '../data/index';

const middleware = [
	thunk,
];

// middleware.push(logger);

// Now you can dispatch navigation actions from anywhere!
// store.dispatch(push('/foo'))

// Add the reducer to your store on the `router` key
// Also apply our middleware for navigating
export const store = createStore(
	reducers(),
	Immutable.Map(),
	compose(
		applyMiddleware(...middleware)
	)
);


export default store;
