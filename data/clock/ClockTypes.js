export default {
	CLOCK_IN:  'CLOCK_IN',
	VERIFY_CLOCK_IN_FAIL: 'VERIFY_CLOCK_IN_FAIL',
	VERIFY_CLOCK_IN_SUCCESS: 'VERIFY_CLOCK_IN_SUCCESS',
	RESET_CLOCK: 'RESET_CLOCK',
	CLOCK_OUT: 'CLOCK_OUT',
	UPDATE_TIME: 'UPDATE_TIME',
	VERIFY_CLOCK_OUT_FAIL: 'VERIFY_CLOCK_OUT_FAIL',
	VERIFY_CLOCK_OUT_SUCCESS: 'VERIFY_CLOCK_OUT_SUCCESS',
}
