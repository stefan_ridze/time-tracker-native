function nonEnumerableProperty(value) {
	// The field `enumerable` is `false` by default.
	return {
		value,
		writable: true,
		configurable: true,
	};
}

export class APIError extends Error {
	constructor(message, code = '', description = null, stack = false) {
		super();

		this.message = message;
		this.description = description;
		if (stack) {
			this.stack = stack;
		}
		this.code = code;

		if (process.env.NODE_ENV !== 'production') {
			if ('captureStackTrace' in Error) {
				// V8 specific method. (chrome, node, etc)
				Error.captureStackTrace(this, this.constructor);
			} else {
				// Generic way to set the error stack trace. (firefox)
				Object.defineProperty(this, 'stack', nonEnumerableProperty(Error(message).stack));
			}
		}
	}

	toJSON() {
		const error = {
			message: this.message,
			description: this.description,
			stack: (this.stack || '').split('\n'),
			code: this.code,
		};

		return error;
	}
}

export function errorToJSON(error) {
	if (typeof error.toJSON !== 'undefined') {
		return error.toJSON();
	}
	if (typeof error.message !== 'undefined') {
		return {
			message: error.message,
		};
	}

	// Need to see how to solve this case
	return error;
}
