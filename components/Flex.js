import styled from 'styled-components';

export const FlexRow = styled.View`
	flex-direction: row;
`;

export const FlexRowSpaceAround = styled(FlexRow)`
	justify-content: space-around;
`;

export const FlexRowSpaceBetween = styled(FlexRow)`
	justify-content: space-between;
`;
