import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

// Screens
import SignUp from '../screens/SignUp';
import SignIn from '../screens/SignIn';
import Dashboard from '../screens/Dashboard';
import Reports from '../screens/Reports';

// TODO Layouts and Switch Router
import SignedInLayout from '../layouts/SignedInLayout';

const renderSignedInLayout = (screen, props) => (
	<SignedInLayout {...props}>
		{screen}
	</SignedInLayout>
);

const MainNavigator = createStackNavigator({
	SignUp,
	SignIn,
	Dashboard: props => renderSignedInLayout(<Dashboard {...props} />, props),
	Reports: props => renderSignedInLayout(<Reports {...props} />, props),
}, {
	initialRouteName: 'Dashboard',
	headerMode: 'none',
});

const MainRouter = createAppContainer(MainNavigator);

export default MainRouter;
