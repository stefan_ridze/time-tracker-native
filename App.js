import React from 'react';
import styled from 'styled-components';
import { Provider } from 'react-redux';

import { store } from './lib/store';

import MainRouter from './routers/MainRouter';

const OuterWrapper = styled.View`
	min-width: 100%;
	min-height: 100%;
	padding-top: 24px;
`;

const TimeTracker = () => (
	<OuterWrapper>
		<Provider store={store}>
			<MainRouter />
		</Provider>
	</OuterWrapper>
);

export default TimeTracker;
