import React from 'react';
import styled from 'styled-components';

// Style
import Colors from '../style/Colors';

// Components
import { FlexRowSpaceAround } from './Flex';

const TopPictureWrapper = styled(FlexRowSpaceAround)`
	width: 100%;
	background-color: ${Colors.baseDark};
	position: relative;
`;

export const Picture = styled.ImageBackground`
	width: ${props => props.width || '375px'};
	height: ${props => props.height || '211px'};
`;

const TopPicture = ({ source, width, height }) => (
	<TopPictureWrapper>
		<Picture source={source} width={width} height={height} />
	</TopPictureWrapper>
);

export default TopPicture;
