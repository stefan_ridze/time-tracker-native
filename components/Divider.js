import styled from 'styled-components';

export const BaseDivider = styled.View`
	width: 100%;
	height: ${props => (props.height || '0px')}
`;
