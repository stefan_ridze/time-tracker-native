import React, { Fragment } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import { connect } from 'react-redux';

// Style
import Colors from '../style/Colors';
import FontSizes from '../style/FontSizes';

// Components
import { FlexRow } from './Flex';

const ClockTextWrapper = styled(FlexRow)`
	align-items: center;
`;

const ClockText = styled.Text`
	font-size: 100px;
	color: ${Colors.darkGrey};
	line-height: 100px;
`;

const Dots = styled(ClockText)`
	align-self: flex-start;
	margin-top: -5px;
`;

const ClockTime = styled.View`
	align-items: center;
`;

const TimeCaption = styled.Text`
	font-size: ${FontSizes.big};
	color: ${Colors.darkGrey};
`;

const ensureTwoDigits = number => (number <= 9 ? `0${number}` : number);

const Clock = ({ clockedInTime, currentTime }) => {
	let seconds = '00';
	let minutes = '00';

	if (clockedInTime) {
		const duration = moment.duration(moment(currentTime).diff(clockedInTime));
		seconds = ensureTwoDigits(duration.get('seconds').toString());
		minutes =  ensureTwoDigits(duration.get('minutes').toString());
		console.log('SECONDS', seconds);
	}

	return (
		<Fragment>
			<ClockTextWrapper>
				<ClockTime>
					<ClockText>{minutes}</ClockText>
					<TimeCaption>HOURS</TimeCaption>
				</ClockTime>
				<Dots>:</Dots>
				<ClockTime>
					<ClockText>{seconds}</ClockText>
					<TimeCaption>MINUTES</TimeCaption>
				</ClockTime>
			</ClockTextWrapper>
		</Fragment>
	);
};

function mapStateToProps(state) {
	return {
		clockedInTime: state.getIn(['clock', 'clockedInTime']),
		currentTime: state.getIn(['clock', 'currentTime']),
	}
}

export default connect(mapStateToProps, null)(Clock);
