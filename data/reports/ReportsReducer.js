import { fromJS } from 'immutable';
import REPORT_TYPES from './ReportsTypes';

const initialState = fromJS({
	reports: [],
	loading: false,
	error: null,
});

export default function reportsReducer(state = initialState, action) {
	switch (action.type) {
	case REPORT_TYPES.FETCH_REPORTS: {
		return state.set('loading', true);
	}
	case REPORT_TYPES.FETCH_REPORTS_FAIL: {
		return state
			.set('loading', false)
			.set('error', action.payload);
	}
	case REPORT_TYPES.FETCH_REPORTS_SUCCESS: {
		const reports = action.payload;
		return state
			.set('loading', false)
			.set('reports', fromJS(reports));
	}
	default: {
		return state;
	}
	}
}
