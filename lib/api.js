import axios from 'axios';
import { APIError } from './Errors';
import { AsyncStorage } from 'react-native';

const REACT_APP_API_URL='http://192.168.0.37:5000';

axios.defaults.withCredentials = true;

class Api {
	static async headers() {
		const accessToken = await AsyncStorage.getItem('accessToken');
		return {
			Accept: 'application/json',
			'Content-Type': 'application/json',
			'token': accessToken,
		};
	}

	/**
	 * Fetch a url from the API
	 * @param {string} url
	 * @returns {Promise<*>}
	 */
	static get(url, config = {}) {
		return this.xhr('get', url, {}, config);
	}

	static post(url, data = {}, config = {}) {
		return this.xhr('post', url, data, config);
	}

	static put(url, data = {}, config = {}) {
		return this.xhr('put', url, data, config);
	}

	static delete(url) {
		return this.xhr('delete', url);
	}

	/**
	 * Base fetch method. Should not be used directly, but through above functions.
	 * @param method
	 * @param url
	 * @returns {Promise<*>}
	 */
	static async xhr(method, url, data = {}, config = {}) {
		try {
			const headers = {
				...await this.headers(),
				...(config.headers || {}),
			};
			const options = {
				method,
				data,
				...config,
				headers,
				url: `${REACT_APP_API_URL}/api/${url}`,
			};
			const xhrResult = await axios(options);
			// console.log('url:', `${REACT_APP_API_URL}/api/${url}`);
			// console.log('RESPONSE', xhrResult);
			return xhrResult.data;
		} catch (error) {
			console.log('Exception in API call', method, url);
			// @NOTE:ezelohar Maybe I can move error handlers here and notification displaying?
			if (error.response) {
				console.log('HTTP error', error.response);
				throw new APIError(error.response.statusText, error.response.status);
			} else {
				console.log('Network error', error.message, error.status, error.code);
				throw new APIError(error.message);
			}
		}
	}
}

export default Api;
