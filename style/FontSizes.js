export default {
	base: '16px',
	small: '14px',
	medium: '18px',
	big: '25px',
}
