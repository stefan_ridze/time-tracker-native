import USER_TYPES from './UserTypes';
import Api from '../../lib/api';
import { AsyncStorage } from 'react-native';

function loginFail(error) {
	return dispatch => dispatch({
		type: USER_TYPES.LOGIN_FAIL,
		payload: error,
	});
}

function loginSuccessful(user) {
	return dispatch => dispatch({
		type: USER_TYPES.LOGIN_SUCCESS,
		payload: user,
	});
}

export function login(email, password) {
	return async (dispatch) => {
		try {
			const user = await Api.post('users/sign-in', {
				email,
				password,
			});
			dispatch(loginSuccessful(user));
			const { accessToken } = user;
			AsyncStorage.setItem('accessToken', accessToken);
			return user;
		} catch (error) {
			dispatch(loginFail(error));
			throw error;
		}
	}
}

function logoutFail(error) {
	return dispatch => dispatch({
		type: USER_TYPES.LOGOUT_FAIL,
		payload: error,
	});
}

function logoutSuccessful() {
	return dispatch => dispatch({
		type: USER_TYPES.LOGOUT_SUCCESS,
	});
}

export function logout() {
	return async (dispatch) => {
		try {
			await Api.post('users/sign-out');
			AsyncStorage.removeItem('accessToken');
			return dispatch(logoutSuccessful());
		} catch (error) {
			dispatch(logoutFail(error));
			throw error;
		}
	}
}

function signUpFail(error) {
	return dispatch => dispatch({
		type: USER_TYPES.SIGN_UP_FAIL,
		payload: error,
	});
}

function signUpSuccessful() {
	return dispatch => dispatch({
		type: USER_TYPES.SIGN_UP_SUCCESS,
	});
}

export function signUp(email, password, fullName) {
	return async (dispatch) => {
		try {
			await Api.post('users/sign-up', {
				fullName,
				email,
				password,
			});
			return dispatch(signUpSuccessful());
		} catch (error) {
			return dispatch(signUpFail(error));
		}
	}
}
