import React, { Fragment } from 'react';
import styled from 'styled-components';

// Pictures
import circleSmall from '../public/pictures/circle-small.png';

// Styles
import Colors from '../style/Colors';

// Components
import Header from '../components/Header';
import CurrentDate from '../components/CurrentDate';
import { BaseDivider } from '../components/Divider';

const ScreenWrapper = styled.View`
	width: 100%;
	min-height: 100%;
	background: ${Colors.white};
	padding: 0 20px 15px 20px;
`;

const SignedInLayout = (props) => {
	const { children } = props;
	return (
		<Fragment>
			<Header {...props} picture={circleSmall} />
			<BaseDivider height="20px" />
			<ScreenWrapper>
				<CurrentDate />
				<BaseDivider height="30px" />
				{children}
			</ScreenWrapper>
		</Fragment>
	);
};

export default SignedInLayout;
