import React from 'react';
import styled from 'styled-components';

// style
import Colors from '../style/Colors';

const Button = styled.Button``;

const ButtonWrapper = styled.View`
	border-radius: 12px;
	overflow: hidden;
`;

const BaseButton = ({ onPress, title, bigger, backgroundColor }) => {
	return (
		<ButtonWrapper bigger={bigger}>
			<Button
				color={backgroundColor || Colors.baseGreen}
				bigger={bigger}
				onPress={onPress}
				title={title}
			/>
		</ButtonWrapper>
	);
};

export default BaseButton;
