import styled from 'styled-components';

// Style
import Colors from '../style/Colors';
import FontSizes from '../style/FontSizes';

export const BaseText = styled.Text`
	color: ${props => props.color || Colors.white};
	font-size: ${props => props.fontSize || FontSizes.base};
	font-weight: ${props => props.fontWeight || 300};
`;

export const Heading = styled.Text`
	color: ${Colors.mediumGrey};
	font-size: ${FontSizes.big};
`;
