import React from 'react';
import styled from 'styled-components';

// Style
import Colors from '../style/Colors';
import PictureDimensions from '../style/PictureDimensions';

// Components
import { Picture } from './TopPicture';
import { FlexRowSpaceAround } from './Flex';

const HeaderWrapper = styled(FlexRowSpaceAround)`
	background: ${Colors.baseDark};
	position: relative;
`;

const Button = styled.Button`
	color: ${Colors.white};
	font-size: 20px;
`;

const ButtonWrapper = styled.View`
	position: absolute;
	${props => props.left && ('left: 10px;')};
	${props => props.right && ('right: 10px;')};
`;

const PictureWrapper = styled.View``;

const Header = (props) => {
	const { picture, navigation } = props;
	const { navigate } = navigation;

	const signOut = async () => {
		await actions.logout();
		navigate('SignIn');
	};

	const goBack = () => navigation.goBack();

	const navigationStack = navigation.dangerouslyGetParent();
	const canGoBack = navigationStack && navigationStack.state && navigationStack.state.index !== 1;

	const {
		width, height,
	} = PictureDimensions.small;

	console.log('RENDER HEADER, HAS BACK BUTTON:', canGoBack);
	return (
		<HeaderWrapper>
			{canGoBack && (
				<ButtonWrapper left>
					<Button
						left
						color={Colors.baseDark}
						title="BACK"
						onPress={goBack}
					/>
				</ButtonWrapper>
			)}
			<PictureWrapper>
				<Picture
					source={picture}
					width={width}
					height={height}
				/>
			</PictureWrapper>
			<ButtonWrapper right>
				<Button
					right
					color={Colors.baseDark}
					title="SIGN OUT"
					onPress={signOut}
				/>
			</ButtonWrapper>
		</HeaderWrapper>
	);
};

export default Header;
