export default {
	baseDark: '#202630',
	baseGreen: '#06C383',
	baseRed: '#EB6363',
	white: '#ffffff',
	darkGrey: '#222222',
	mediumGrey: '#555555',
	lightGrey: '#999999',
	lighterGrey: '#e6e6e6',
}
