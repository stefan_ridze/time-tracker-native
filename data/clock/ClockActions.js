import CLOCK_TYPES from './ClockTypes';
import Api from '../../lib/api';

export function clockInLocal(clockedInTime) {
	return dispatch => dispatch({
		type: CLOCK_TYPES.CLOCK_IN,
		payload: clockedInTime,
	});
}

export function clockOutLocal(clockedOutTime) {
	return dispatch => dispatch({
		type: CLOCK_TYPES.CLOCK_OUT,
		payload: clockedOutTime,
	});
}


export function verifyClockInFail(error) {
	return dispatch => dispatch({
		type: CLOCK_TYPES.VERIFY_CLOCK_IN_FAIL,
		payload: error,
	});
}

export function verifyClockInSuccessful() {
	return dispatch => dispatch({
		type: CLOCK_TYPES.VERIFY_CLOCK_IN_SUCCESS,
	});
}

export function verifyClockoutFail(error) {
	return dispatch => dispatch({
		type: CLOCK_TYPES.VERIFY_CLOCK_OUT_FAIL,
		payload: error,
	});
}

export function verifyClockoutSuccess() {
	return dispatch => dispatch({
		type: CLOCK_TYPES.VERIFY_CLOCK_OUT_SUCCESS,
	});
}


export function updateTime(currentTime) {
	return dispatch => dispatch({
		type: CLOCK_TYPES.UPDATE_TIME,
		payload: currentTime,
	});
}

export function clockIn(clockedInTime) {
	return async (dispatch) => {
		try {
			dispatch(clockInLocal(clockedInTime));
			 await Api.put('users/clock-in', { clockedInTime });
			return dispatch(verifyClockInSuccessful());
		} catch (error) {
			return dispatch(verifyClockInFail(error))
		}
	};
}

export function clockOut(clockedOutTime) {
	return async (dispatch) => {
		try {
			dispatch(clockOutLocal(clockedOutTime));
			await Api.put('users/clock-out', { clockedOutTime });
			return dispatch(verifyClockoutSuccess(clockedOutTime));
		} catch (error) {
			return verifyClockoutFail();
		}
	}
}
