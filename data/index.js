import { combineReducers } from 'redux-immutable';

// Reducers
import userReducer from './user/UserReducer';
import clockReducer from './clock/ClockReducer';
import reportsReducer from './reports/ReportsReducer';

export default () => combineReducers({
	user: userReducer,
	clock: clockReducer,
	reports: reportsReducer,
});
