import REPORT_TYPES from './ReportsTypes';
import Api from '../../lib/api';

function fetchReportsFail(error) {
	return dispatch => dispatch({
		type: REPORT_TYPES.FETCH_REPORTS_FAIL,
		payload: error,
	});
}

function fetchReportsSuccess(reports) {
	return dispatch => dispatch({
		type: REPORT_TYPES.FETCH_REPORTS_SUCCESS,
		payload: reports,
	});
}

export function fetchReports() {
	return async (dispatch) => {
		try {
			const reports = await Api.get('reports');
			return dispatch(fetchReportsSuccess(reports));
		} catch (error) {
			return dispatch(fetchReportsFail(error));
		}
	}
}
