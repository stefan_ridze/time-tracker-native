import React, { Fragment, useState } from 'react';
import styled from 'styled-components';

// Components
import { BaseText } from './Text';

// style
import Colors from '../style/Colors';
import FontSizes from '../style/FontSizes';

const Input = styled.TextInput`
	width: 100%;
	padding: 5px 0;
	font-size: ${FontSizes.base};
	color: ${Colors.lightGrey};
	border-bottom-color: ${props => props.isFocused ?  Colors.baseGreen : Colors.lightGrey};
	border-bottom-width: 2px;
`;

const Label = styled(BaseText)`
	font-weight: bold;
	color: ${props => props.isFocused ? Colors.baseGreen : Colors.white};
	padding: 0;
	margin: 0;
`;

const BaseInput = (props) => {
	const [isFocused, setFocus] = useState(false);

	const onFocus = () => setFocus(true);
	const onBlur = () => setFocus(false);

	const {
		value,
		placeholder,
		label,
		onChangeText,
		secureTextEntry,
	} = props;

	return (
		<Fragment>
			{label && (<Label isFocused={isFocused}>{label}</Label>)}
			<Input
				secureTextEntry={secureTextEntry}
				value={value}
				onChangeText={onChangeText}
				placeholder={placeholder}
				onFocus={onFocus}
				onBlur={onBlur}
				isFocused={isFocused}
			/>
		</Fragment>
	);
};

export default BaseInput;
